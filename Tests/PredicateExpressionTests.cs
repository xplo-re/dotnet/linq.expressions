/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System.Diagnostics.CodeAnalysis;
using System.Linq.Expressions;
using FluentAssertions;
using Xunit;


namespace XploRe.Linq.Expressions.Tests
{

    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    public class PredicateExpressionTests
    {

        [Fact]
        public void False()
        {
            var lambda = PredicateExpression.False<object>();

            lambda.Should().NotBeNull();
            lambda.Parameters.Should().HaveCount(1, "because this predicate has one argument");
            lambda.Parameters[0].Type.Should().Be<object>("because predicate was created with type parameter object");
            lambda.Body.Should().BeOfType<ConstantExpression>("because base predicate yields a constant value");

            // Verify that constant expression body is false.
            ((ConstantExpression) lambda.Body).Is(false).Should().BeTrue("because predicate value must be false");

            // Verify that compiled invocation yield false.
            lambda.Compile().Invoke(null).Should().BeFalse("because predicate should always yield false");
        }

        [Fact]
        public void True()
        {
            var lambda = PredicateExpression.True<object>();

            lambda.Should().NotBeNull();
            lambda.Parameters.Should().HaveCount(1, "because this predicate has one argument");
            lambda.Parameters[0].Type.Should().Be<object>("because predicate was created with type parameter object");
            lambda.Body.Should().BeOfType<ConstantExpression>("because base predicate yields a constant value");

            // Verify that constant expression body is true.
            ((ConstantExpression) lambda.Body).Is(true).Should().BeTrue("because predicate value must be true");

            // Verify that compiled invocation yield true.
            lambda.Compile().Invoke(null).Should().BeTrue("because predicate should always yield true");
        }

    }

}
