/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using XploRe.Collections;
using Xunit;


namespace XploRe.Linq.Expressions.Tests
{

    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    public class ConstantExpressionExtensions
    {

        [Fact]
        public void IsWithValueType()
        {
            var constant = Expression.Constant(true);

            constant.Is<bool>().Should().BeTrue("because types match");
            constant.Is<object>().Should().BeTrue("because Boolean is derived from Object");
            constant.Is<ValueType>().Should().BeTrue("because Boolean is derived from ValueType");

            constant.Is<int>().Should().BeFalse("because Int32 is not assignable to Boolean");
            constant.Is<string>().Should().BeFalse("because String is not assignable to Boolean");

            constant.Is(true).Should().BeTrue("because value and type match");
            constant.Is(false).Should().BeFalse("because type matches but value doesn't");
            constant.Is((char) 1).Should().BeFalse("because Char is not assignable to Boolean");
        }

        [Fact]
        public void IsWithReferenceType()
        {
            const string numericTestString = "42";
            var constant = Expression.Constant(numericTestString);

            constant.Is<string>().Should().BeTrue("because types match");
            constant.Is<object>().Should().BeTrue("because String is derived from Object");
            constant.Is<IEnumerable>().Should().BeTrue("because String implemented IEnumerable");
            constant.Is<IEnumerable<char>>().Should().BeTrue("because String implemented IEnumerable<char>");
            constant.Is<IEnumerable<int>>().Should().BeFalse("because String does not implemented IEnumerable<int>");

            constant.Is(numericTestString).Should().BeTrue("because value and type match");
            constant.Is(numericTestString.Reverse()).Should().BeFalse("because type matches but value doesn't");
            constant.Is<string>(null).Should().BeFalse("because type matches but value is null");
        }

        [Fact]
        public void IsWithReferenceNull()
        {
            var constant = Expression.Constant(null);

            constant.Is<object>().Should().BeTrue("because type for null constant is object");

            constant.Is<object>(null).Should().BeTrue("because type and value match");
            constant.Is<object>(string.Empty).Should().BeFalse("because type matches but value doesn't");
            constant.Is<string>(null).Should().BeFalse("because values matches but type doesn't");
        }

        [Fact]
        public void IsWithReferenceEqualityComparer()
        {
            var valueTypeConstant = Expression.Constant(true);

            valueTypeConstant.Is(true, EqualityComparer<bool>.Default).Should().BeTrue();
            valueTypeConstant.Is(true, ReferenceEqualityComparer.Instance).Should().BeFalse(
                "because boxed comparison value is a new reference with reference equality requested"
            );
        }

    }

}
