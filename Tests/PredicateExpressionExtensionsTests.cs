/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using FluentAssertions;
using Xunit;


#pragma warning disable 162

namespace XploRe.Linq.Expressions.Tests
{

    [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
    public class PredicateExpressionExtensionsTests
    {

        [SuppressMessage("ReSharper", "CoVariantArrayConversion")]
        [SuppressMessage("ReSharper", "ReplaceWithSingleCallToAny")]
        [SuppressMessage("ReSharper", "UnusedMember.Local")]
        private class Generator
        {

            public const string ExampleTestString = "Test String X 42";

            public static IEnumerable<object[]> AndExpressionResults
            {
                // Use same name for result parameter to simply comparisons.
                get {
                    yield return new Expression<Func<string, bool>>[] {
                        s => s.Length > 1,
                        o => o.Contains("X"),
                        s => s.Length > 1 && s.Contains("X")
                    };

                    yield return new Expression<Func<string, bool>>[] {
                        s => s.ToArray().Where(x => x == 'c').Any(),
                        o => o.ToCharArray().Any(z => z.GetHashCode() < 25),
                        s => s.ToArray().Where(x => x == 'c').Any() && s.ToCharArray().Any(z => z.GetHashCode() < 25)
                    };

                    yield return new Expression<Func<string, bool>>[] {
                        s => true,
                        o => o.Contains("X"),
                        // Parameter name change, as during optimisation the second predicate is returned verbatim.
                        o => o.Contains("X")
                    };

                    yield return new Expression<Func<string, bool>>[] {
                        s => false,
                        o => o.Contains("X"),
                        s => false && s.Contains("X")
                    };
                }
            }

            public static IEnumerable<object[]> OrExpressionResults
            {
                // Use same name for result parameter to simply comparisons.
                get {
                    yield return new Expression<Func<string, bool>>[] {
                        s => s.Length > 1,
                        o => o.Contains("X"),
                        s => s.Length > 1 || s.Contains("X")
                    };

                    yield return new Expression<Func<string, bool>>[] {
                        s => s.ToArray().Where(x => x == 'c').Any(),
                        o => o.ToCharArray().Any(z => z.GetHashCode() < 25),
                        s => s.ToArray().Where(x => x == 'c').Any() || s.ToCharArray().Any(z => z.GetHashCode() < 25)
                    };

                    yield return new Expression<Func<string, bool>>[] {
                        s => false,
                        o => o.Contains("X"),
                        // Parameter name change, as during optimisation the second predicate is returned verbatim.
                        o => o.Contains("X")
                    };

                    yield return new Expression<Func<string, bool>>[] {
                        s => true,
                        o => o.Contains("X"),
                        s => true || s.Contains("X")
                    };
                }
            }

        }

        [Fact]
        public void AndOnConstantTrue()
        {
            Expression<Func<object, bool>> starter = s => true;
            Expression<Func<object, bool>> other = x => x.Equals(x.GetType());

            starter.And(other).Should().BeSameAs(other, "because constant true body is removed during optimisation");
        }

        [Fact]
        public void OrOnConstantFalse()
        {
            Expression<Func<object, bool>> starter = s => false;
            Expression<Func<object, bool>> other = x => x.Equals(x.GetType());

            starter.Or(other).Should().BeSameAs(other, "because constant false body is removed during optimisation");
        }

        [Theory]
        [MemberData(nameof(Generator.AndExpressionResults), MemberType = typeof(Generator))]
        public void AndResultShape(
            Expression<Func<string, bool>> starter,
            Expression<Func<string, bool>> other,
            Expression<Func<string, bool>> result)
        {
            var expression = starter.And(other);

            expression.Should().BeEquivalentTo(result);
        }

        [Theory]
        [MemberData(nameof(Generator.AndExpressionResults), MemberType = typeof(Generator))]
        public void AndResultScope(
            Expression<Func<string, bool>> starter,
            Expression<Func<string, bool>> other,
            Expression<Func<string, bool>> result)
        {
            var expression = starter.And(other);

            // Check whether the expected result expression can be compiled.
            Func<bool> resultAction = () => result.Compile().Invoke(Generator.ExampleTestString);

            resultAction.Should().NotThrow("because the sample data must be correct");

            // Validate whether the resulting expression can be compiled and catches the scope correctly.
            Func<bool> expressionAction = () => expression.Compile().Invoke(Generator.ExampleTestString);

            expressionAction.Should().NotThrow("because parameters should be rewritten into a valid expression");

            // Outputs of expected result expression and joined expression should match.
            expressionAction.Invoke().Should().Be(resultAction.Invoke(), "because outputs should match");
        }

        [Theory]
        [MemberData(nameof(Generator.OrExpressionResults), MemberType = typeof(Generator))]
        public void OrResultShape(
            Expression<Func<string, bool>> starter,
            Expression<Func<string, bool>> other,
            Expression<Func<string, bool>> result)
        {
            var expression = starter.Or(other);

            expression.Should().BeEquivalentTo(result);

            // Validate whether the resulting expression catches the scope correctly.
            Action task = () => expression.Compile().Invoke("Test String");

            task.Should().NotThrow("because parameters should be rewritten into a valid expression");
        }

        [Theory]
        [MemberData(nameof(Generator.OrExpressionResults), MemberType = typeof(Generator))]
        public void OrResultScope(
            Expression<Func<string, bool>> starter,
            Expression<Func<string, bool>> other,
            Expression<Func<string, bool>> result)
        {
            var expression = starter.Or(other);

            // Check whether the expected result expression can be compiled.
            Func<bool> resultAction = () => result.Compile().Invoke(Generator.ExampleTestString);

            resultAction.Should().NotThrow("because the sample data must be correct");

            // Validate whether the resulting expression can be compiled and catches the scope correctly.
            Func<bool> expressionAction = () => expression.Compile().Invoke(Generator.ExampleTestString);

            expressionAction.Should().NotThrow("because parameters should be rewritten into a valid expression");

            // Outputs of expected result expression and joined expression should match.
            expressionAction.Invoke().Should().Be(resultAction.Invoke(), "because outputs should match");
        }

    }

}
