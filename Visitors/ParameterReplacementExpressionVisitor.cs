/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using JetBrains.Annotations;
using XploRe.Collections;


namespace XploRe.Linq.Expressions.Visitors
{

    /// <inheritdoc />
    /// <summary>
    ///     Specialised expression visitor that replaces all <see cref="ParameterExpression" /> nodes with a provided
    ///     replacement expression.
    /// </summary>
    [PublicAPI]
    public class ParameterReplacementExpressionVisitor : ExpressionVisitor
    {

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="ParameterReplacementExpressionVisitor" /> with a set of parameter expressions
        ///     and their corresponding replacement expressions.
        /// </summary>
        /// <param name="parameters">Sequence of <see cref="ParameterExpression" /> nodes to replace.</param>
        /// <param name="replacements">
        ///     Sequence of replacement expressions. Must be of the same length as <paramref name="parameters" />. Each
        ///     parameter from <paramref name="parameters" /> is replaced with the provided replacement expression at
        ///     the same sequence index.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The length of the <paramref name="replacements" /> sequence does not match the length of the
        ///     <paramref name="parameters" /> sequence.
        /// </exception>
        [SuppressMessage("ReSharper", "ConstantConditionalAccessQualifier")]
        public ParameterReplacementExpressionVisitor(
            [NotNull] [ItemNotNull] IEnumerable<ParameterExpression> parameters,
            [NotNull] [ItemNotNull] IEnumerable<Expression> replacements)
            : this(parameters?.ToArray(), replacements?.ToArray())
        {
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="ParameterReplacementExpressionVisitor" /> with a set of parameter expressions
        ///     and their corresponding replacement expressions.
        /// </summary>
        /// <param name="parameters">Array of <see cref="ParameterExpression" /> nodes to replace.</param>
        /// <param name="replacements">
        ///     Array of replacement expressions. Must be of the same length as <paramref name="parameters" />. Each
        ///     parameter from <paramref name="parameters" /> is replaced with the provided replacement expression at
        ///     the same array index.
        /// </param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     The length of the <paramref name="replacements" /> array  does not match the length of the
        ///     <paramref name="parameters" /> array.
        /// </exception>
        public ParameterReplacementExpressionVisitor(
            [NotNull] [ItemNotNull] ParameterExpression[] parameters,
            [NotNull] [ItemNotNull] Expression[] replacements)
        {
            if (parameters == null) {
                throw new ArgumentNullException(nameof(parameters));
            }

            if (replacements == null) {
                throw new ArgumentNullException(nameof(replacements));
            }

            if (parameters.Length != replacements.Length) {
                throw new ArgumentOutOfRangeException(
                    nameof(replacements),
                    "Number of replacement expressions does not match the number of parameter expressions."
                );
            }

            _replacements = Enumerable.Range(0, parameters.Length)
                                      .ToDictionary<int, ParameterExpression, Expression>(
                                          i => parameters[i],
                                          i => replacements[i],
                                          ReferenceEqualityComparer.Instance
                                      );
        }

        /// <inheritdoc />
        /// <summary>
        ///     Initialises a new <see cref="ParameterReplacementExpressionVisitor" /> with a dictionary that maps
        ///     parameter expressions to their corresponding replacement expressions.
        /// </summary>
        /// <param name="replacements">
        ///     Dictionary of <see cref="ParameterExpression" />s as keys and their corresponding replacement expressions
        ///     as values.
        /// </param>
        public ParameterReplacementExpressionVisitor(
            [NotNull] IDictionary<ParameterExpression, Expression> replacements)
        {
            if (replacements == null) {
                throw new ArgumentNullException(nameof(replacements));
            }

            _replacements = replacements;
        }

        /// <inheritdoc />
        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (_replacements.TryGetValue(node, out var replacement)) {
                return replacement;
            }

            return base.VisitParameter(node);
        }

        [NotNull]
        private IDictionary<ParameterExpression, Expression> _replacements;

    }

}
