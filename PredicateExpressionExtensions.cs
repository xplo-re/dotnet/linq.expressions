/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Linq.Expressions;
using JetBrains.Annotations;
using XploRe.Linq.Expressions.Visitors;
using XploRe.Runtime;


namespace XploRe.Linq.Expressions
{

    /// <summary>
    ///     Extension methods for building predicate expressions.
    /// </summary>
    [PublicAPI]
    public static class PredicateExpressionExtensions
    {

        /// <summary>
        ///     Merges two predicate expressions into a single, new predicate expression under a conditional AND
        ///     operation.
        ///     <para>
        ///     If <paramref name="this" /> predicate expression is a constant starter predicate (i.e. the body is a
        ///     constant <c>true</c> expression), the <paramref name="other" /> predicate expression is returned instead.
        ///     This optimisation allows to dynamically build a conjunction via simplified chaining starting with an
        ///     expression such as <see cref="PredicateExpression.True{T}" />.
        ///     </para>
        /// </summary>
        /// <param name="this">
        ///     This predicate expression whose body is used on the left side of the conditional AND operation.
        /// </param>
        /// <param name="other">
        ///     A predicate expression whose body is used on the right side of the conditional AND operation.
        /// </param>
        /// <typeparam name="T">The parameter type of the predicate expressions.</typeparam>
        /// <returns>
        ///     A new predicate expression with both provided expression bodies joined under a conditional AND operation,
        ///     or the <paramref name="other" /> expression in case <paramref name="this" /> expression is a constant
        ///     starter predicate that yields <c>true</c>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="this" /> expression or the <paramref name="other" /> expression is <c>null</c>.
        /// </exception>
        [Pure]
        [NotNull]
        public static Expression<Func<T, bool>> And<T>(
            [NotNull] this Expression<Func<T, bool>> @this,
            [NotNull] Expression<Func<T, bool>> other)
        {
            if (@this == null) {
                throw new ArgumentNullException(nameof(@this));
            }

            if (other == null) {
                throw new ArgumentNullException(nameof(other));
            }

            // Check for default implementation.
            var parameters = @this.Parameters;

            if (@this.Body is ConstantExpression constant && constant.Is(true)) {
                // Skip initial constant start expression.
                return other;
            }

            // Adjust expression parameters.
            var visitor = new ParameterReplacementExpressionVisitor(other.Parameters, replacements: parameters);

            var updatedExpressionBody = visitor.Visit(other.Body);

            if (updatedExpressionBody == null) {
                throw new RuntimeException("Parameter replacement returned null expression.");
            }

            // Keep parameters of this expression and return new lambda.
            return Expression.Lambda<Func<T, bool>>(
                Expression.AndAlso(@this.Body, updatedExpressionBody),
                parameters
            );
        }

        /// <summary>
        ///     Merges two predicate expressions into a single, new predicate expression under a conditional OR
        ///     operation.
        ///     <para>
        ///     If <paramref name="this" /> predicate expression is a constant starter predicate (i.e. the body is a
        ///     constant <c>false</c> expression), the <paramref name="other" /> predicate expression is returned instead.
        ///     This optimisation allows to dynamically build a disjunction via simplified chaining starting with an
        ///     expression such as <see cref="PredicateExpression.False{T}" />.
        ///     </para>
        /// </summary>
        /// <param name="this">
        ///     This predicate expression whose body is used on the left side of the conditional OR operation.
        /// </param>
        /// <param name="other">
        ///     A predicate expression whose body is used on the right side of the conditional OR operation.
        /// </param>
        /// <typeparam name="T">The parameter type of the predicate expressions.</typeparam>
        /// <returns>
        ///     A new predicate expression with both provided expression bodies joined under a conditional OR operation,
        ///     or the <paramref name="other" /> expression in case <paramref name="this" /> expression is a constant
        ///     starter predicate that yields <c>false</c>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="this" /> expression or the <paramref name="other" /> expression is <c>null</c>.
        /// </exception>
        [Pure]
        [NotNull]
        public static Expression<Func<T, bool>> Or<T>(
            [NotNull] this Expression<Func<T, bool>> @this,
            [NotNull] Expression<Func<T, bool>> other)
        {
            if (@this == null) {
                throw new ArgumentNullException(nameof(@this));
            }

            if (other == null) {
                throw new ArgumentNullException(nameof(other));
            }

            // Check for default implementation.
            var parameters = @this.Parameters;

            if (@this.Body is ConstantExpression constant && constant.Is(false)) {
                // Skip initial constant start expression.
                return other;
            }

            // Adjust expression parameters.
            var visitor = new ParameterReplacementExpressionVisitor(other.Parameters, replacements: parameters);

            var updatedExpressionBody = visitor.Visit(other.Body);

            if (updatedExpressionBody == null) {
                throw new RuntimeException("Parameter replacement returned null expression.");
            }

            // Keep parameters of this expression and return new lambda.
            return Expression.Lambda<Func<T, bool>>(
                Expression.OrElse(@this.Body, updatedExpressionBody),
                parameters
            );
        }

    }

}
