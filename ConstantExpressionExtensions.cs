/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using JetBrains.Annotations;


namespace XploRe.Linq.Expressions
{

    /// <summary>
    ///     Extension methods for <see cref="ConstantExpression" />.
    /// </summary>
    [PublicAPI]
    public static class ConstantExpressionExtensions
    {

        /// <summary>
        ///     Checks whether the type of a <see cref="ConstantExpression" /> is compatible with <typeparamref name="T" />,
        ///     i.e. the value of the <see cref="ConstantExpression" /> can be cast to <typeparamref name="T" />.
        /// </summary>
        /// <param name="this">This <see cref="ConstantExpression" /> whose type is checked.</param>
        /// <typeparam name="T">The type to check for.</typeparam>
        /// <returns>
        ///     <c>true</c> if the type of the <see cref="ConstantExpression" /> is compatible to <typeparamref name="T" />,
        ///     otherwise <c>false</c>.
        /// </returns>
        public static bool Is<T>([NotNull] this ConstantExpression @this)
        {
            if (@this == null) {
                throw new ArgumentNullException(nameof(@this));
            }

            return typeof(T).IsAssignableFrom(@this.Type);
        }

        /// <summary>
        ///     Checks whether the <see cref="ConstantExpression" /> is equal to the provided value.
        ///     <para>
        ///     The type of the <see cref="ConstantExpression" /> is checked for compatibility with <typeparamref name="T" />.
        ///     If the types are compatible, the value is compared using the default equality comparer for <typeparamref name="T" />.
        ///     </para>
        /// </summary>
        /// <param name="this">This <see cref="ConstantExpression" /> whose value is checked.</param>
        /// <param name="value">The value to compare to.</param>
        /// <typeparam name="T">The type of the value that is checked for compatibility.</typeparam>
        /// <returns>
        ///     <c>true</c> if the type of the <see cref="ConstantExpression" /> is compatible to <typeparamref name="T" />
        ///     and the values are equal according to the default equality comparer for <typeparamref name="T" />,
        ///     otherwise <c>false</c>.
        /// </returns>
        public static bool Is<T>([NotNull] this ConstantExpression @this, T value)
            => Is(@this, value, EqualityComparer<T>.Default);

        /// <summary>
        ///     Checks whether the <see cref="ConstantExpression" /> is equal to the provided value.
        ///     <para>
        ///     The type of the <see cref="ConstantExpression" /> is checked for compatibility with <typeparamref name="T" />.
        ///     If the types are compatible, the value is compared using the provided equality comparer.
        ///     </para>
        /// </summary>
        /// <param name="this">This <see cref="ConstantExpression" /> whose value is checked.</param>
        /// <param name="value">The value to compare to.</param>
        /// <param name="comparer">An <see cref="IEqualityComparer{T}" /> to determine value equality.</param>
        /// <typeparam name="T">The type of the value that is checked for compatibility.</typeparam>
        /// <returns>
        ///     <c>true</c> if the type of the <see cref="ConstantExpression" /> is compatible to <typeparamref name="T" />
        ///     and the values are equal according to <paramref name="comparer" />, otherwise <c>false</c>.
        /// </returns>
        public static bool Is<T>([NotNull] this ConstantExpression @this, T value, IEqualityComparer<T> comparer)
        {
            if (@this == null) {
                throw new ArgumentNullException(nameof(@this));
            }

            if (!typeof(T).IsAssignableFrom(@this.Type)) {
                return false;
            }

            if (comparer == null) {
                comparer = EqualityComparer<T>.Default;
            }

            return comparer.Equals((T) @this.Value, value);
        }

    }

}
