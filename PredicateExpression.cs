/*
 * xplo.re .NET
 *
 * Copyright (C) 2018, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Linq.Expressions;
using JetBrains.Annotations;


namespace XploRe.Linq.Expressions
{

    /// <summary>
    ///     Predicate expression starters.
    /// </summary>
    [PublicAPI]
    public static class PredicateExpression
    {

        /// <summary>
        ///     Returns a predicate expression with one argument of type <typeparamref name="T" /> that yields <c>true</c>.
        /// </summary>
        /// <returns>
        ///     A new predicate expression with one argument of type <typeparamref name="T" /> that yields <c>true</c>.
        /// </returns>
        public static Expression<Func<T, bool>> True<T>() => e => true;

        /// <summary>
        ///     Returns a predicate expression with one argument of type <typeparamref name="T" /> that yields <c>false</c>.
        /// </summary>
        /// <returns>
        ///     A new predicate expression with one argument of type <typeparamref name="T" /> that yields <c>false</c>.
        /// </returns>
        public static Expression<Func<T, bool>> False<T>() => e => false;

    }

}
